// const express = require('express');
import * as express from 'express';

const app = express();
app.disable('x-powered-by');
app.set('json spaces', 2);

// app.use('/api', routes);
const port = process.env.PORT || 3000;
app.get('/', (req, res) => res.send(`Hello! Typescript, build on local`));
app.listen(port, () => console.log(`Listening on ${port}`));
